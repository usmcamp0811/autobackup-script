#!/bin/bash

# Check if the correct number of arguments have been provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <filelist> <target_directory>"
    exit 1
fi

# The file containing the list of file paths
FILELIST="$1"

# The target directory
TARGETDIR="$2"

# Check if the target directory is a git repository
if [ ! -d "$TARGETDIR/.git" ]
then
    echo "$TARGETDIR is not a git repository. Please verify your setup."
    exit 1
fi

# Loop over the list of files
while read -r filepath
do
    # Construct the destination file path
    destination="${TARGETDIR}${filepath}"
    destination_dir=$(dirname "${destination}")

    # Create directory structure in target directory
    mkdir -p "${destination_dir}"

    # Copy the file
    rsync -a "${filepath}" "${destination}"
done < "$FILELIST"

# Change directory to the git repository
cd "$TARGETDIR"

# Stage all changes
git add .

# Create a commit with the current date and time
COMMITMSG="Automated commit at $(date +%Y-%m-%d\ %H:%M:%S)"
git commit -m "$COMMITMSG"

# You can also push the commit to the remote repository if you want
# git push

